package mams;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map.Entry;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import astra.core.Module;
import astra.term.Funct;
import astra.term.ListTerm;
import astra.term.Primitive;
import astra.term.Term;

public class HALConverter extends Module {
    private static final String HAL_PREFIX = "_";

    ObjectMapper mapper = new ObjectMapper();

    @TERM
    public Funct toHalFunct(String json) {
        try {
            JsonNode tree = mapper.readTree(json);
            Term[] terms = new Term[tree.size()];

            int i = 0;
            Iterator<Entry<String, JsonNode>> it = tree.fields();
            while (it.hasNext()) {
                Entry<String, JsonNode> field = it.next();
                terms[i++] = createTerm(field.getKey(), field.getValue());
            }
            return new Funct("hal", terms);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @TERM
    public Funct toRawFunct(String predicate, String json) {
        try {
            JsonNode tree = mapper.readTree(json);

            // Calculate the number of non_hal fields
            int size = countNonHalFields(tree);
            Term[] terms = new Term[size];

            int i = 0;
            Iterator<Entry<String, JsonNode>> it = tree.fields();
            while (it.hasNext()) {
                Entry<String, JsonNode> field = it.next();
                if (!field.getKey().startsWith(HAL_PREFIX)) {
                    terms[i++] = createTerm(field.getKey(), field.getValue());
                }
            }
            return new Funct(predicate, terms);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @TERM
    public Object toObject(String json, String type) {
        try {
            return mapper.readValue(json, Class.forName(type));
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private int countNonHalFields(JsonNode tree) {
        int size = tree.size();
        Iterator<String> keyIt = tree.fieldNames();
        while (keyIt.hasNext()) {
            if (keyIt.next().startsWith(HAL_PREFIX)) size--;
        }
        return size;
    }

    private Term createTerm(String key, JsonNode node) {
        Term[] terms = null;
        if (node.size() == 0) {
            terms = new Term[1];
            if (node.isLong()) {
                terms[0] = Primitive.newPrimitive(node.asLong());
            } else if (node.isInt()) {
                terms[0] = Primitive.newPrimitive(node.asInt());
            } else if (node.isDouble()) {
                terms[0] = Primitive.newPrimitive(node.asDouble());
            } else if (node.isFloat()) {
                terms[0] = Primitive.newPrimitive(node.floatValue());
            } else if (node.isBoolean()) {
                terms[0] = Primitive.newPrimitive(node.asBoolean());
            } else if (node.isTextual()) {
                terms[0] = Primitive.newPrimitive(node.asText());
            } else if (node.isArray()) {
                ListTerm items = new ListTerm();
                Iterator<Entry<String, JsonNode>> it = node.fields();
                while (it.hasNext()) {
                    Entry<String, JsonNode> field = it.next();
                    items.add(createTerm(field.getKey(), field.getValue()));
                }
                return items;
            } else {
                throw new RuntimeException("[HALConverter] Could not convert: " + node);
            }
        } else {
            terms = new Term[node.size()];
            int i = 0;
            Iterator<Entry<String, JsonNode>> it = node.fields();
            while (it.hasNext()) {
                Entry<String, JsonNode> field = it.next();
                terms[i++] = createTerm(field.getKey(), field.getValue());
            }
        }

        return new Funct(key, terms);
    }
}